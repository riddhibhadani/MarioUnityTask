﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Timeloop.Helper
{
    public enum EventsType
    {
        None = 0,
        PlayerLeftMove = 1,
        PlayerRightMove = 2,
        PlayerJump = 3
    }

    public static class Constant
    {

    }
}
